// To parse this JSON data, do
//
//     final result = resultFromJson(jsonString);

import 'dart:convert';

Result resultFromJson(String str) => Result.fromJson(json.decode(str));

String resultToJson(Result data) => json.encode(data.toJson());

class Result {
  Result({
    this.currencies,
    this.bitcoin,
  });

  Currencies currencies;
  Bitcoin bitcoin;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        currencies: json["currencies"] == null
            ? null
            : Currencies.fromJson(json["currencies"]),
        bitcoin:
            json["bitcoin"] == null ? null : Bitcoin.fromJson(json["bitcoin"]),
      );

  Map<String, dynamic> toJson() => {
        "currencies": currencies == null ? null : currencies.toJson(),
        "bitcoin": bitcoin == null ? null : bitcoin.toJson(),
      };
}

class Bitcoin {
  Bitcoin({
    this.blockchainInfo,
    this.coinbase,
    this.bitstamp,
    this.foxbit,
    this.mercadobitcoin,
  });

  Bitstamp blockchainInfo;
  Coinbase coinbase;
  Bitstamp bitstamp;
  Coinbase foxbit;
  Bitstamp mercadobitcoin;

  factory Bitcoin.fromJson(Map<String, dynamic> json) => Bitcoin(
        blockchainInfo: json["blockchain_info"] == null
            ? null
            : Bitstamp.fromJson(json["blockchain_info"]),
        coinbase: json["coinbase"] == null
            ? null
            : Coinbase.fromJson(json["coinbase"]),
        bitstamp: json["bitstamp"] == null
            ? null
            : Bitstamp.fromJson(json["bitstamp"]),
        foxbit:
            json["foxbit"] == null ? null : Coinbase.fromJson(json["foxbit"]),
        mercadobitcoin: json["mercadobitcoin"] == null
            ? null
            : Bitstamp.fromJson(json["mercadobitcoin"]),
      );

  Map<String, dynamic> toJson() => {
        "blockchain_info":
            blockchainInfo == null ? null : blockchainInfo.toJson(),
        "coinbase": coinbase == null ? null : coinbase.toJson(),
        "bitstamp": bitstamp == null ? null : bitstamp.toJson(),
        "foxbit": foxbit == null ? null : foxbit.toJson(),
        "mercadobitcoin":
            mercadobitcoin == null ? null : mercadobitcoin.toJson(),
      };
}

class Bitstamp {
  Bitstamp({
    this.name,
    this.format,
    this.last,
    this.buy,
    this.sell,
    this.variation,
  });

  String name;
  List<String> format;
  double last;
  double buy;
  double sell;
  double variation;

  factory Bitstamp.fromJson(Map<String, dynamic> json) => Bitstamp(
        name: json["name"] == null ? null : json["name"],
        format: json["format"] == null
            ? null
            : List<String>.from(json["format"].map((x) => x)),
        last: json["last"] == null ? null : json["last"].toDouble(),
        buy: json["buy"] == null ? null : json["buy"].toDouble(),
        sell: json["sell"] == null ? null : json["sell"].toDouble(),
        variation:
            json["variation"] == null ? null : json["variation"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "format":
            format == null ? null : List<dynamic>.from(format.map((x) => x)),
        "last": last == null ? null : last,
        "buy": buy == null ? null : buy,
        "sell": sell == null ? null : sell,
        "variation": variation == null ? null : variation,
      };
}

class Coinbase {
  Coinbase({
    this.name,
    this.format,
    this.last,
    this.variation,
  });

  String name;
  List<String> format;
  double last;
  double variation;

  factory Coinbase.fromJson(Map<String, dynamic> json) => Coinbase(
        name: json["name"] == null ? null : json["name"],
        format: json["format"] == null
            ? null
            : List<String>.from(json["format"].map((x) => x)),
        last: json["last"] == null ? null : json["last"].toDouble(),
        variation:
            json["variation"] == null ? null : json["variation"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "format":
            format == null ? null : List<dynamic>.from(format.map((x) => x)),
        "last": last == null ? null : last,
        "variation": variation == null ? null : variation,
      };
}

class Currencies {
  Currencies({
    this.source,
    this.usd,
    this.eur,
    this.gbp,
    this.ars,
    this.btc,
  });

  String source;
  Ars usd;
  Ars eur;
  Ars gbp;
  Ars ars;
  Ars btc;

  factory Currencies.fromJson(Map<String, dynamic> json) => Currencies(
        source: json["source"] == null ? null : json["source"],
        usd: json["USD"] == null ? null : Ars.fromJson(json["USD"]),
        eur: json["EUR"] == null ? null : Ars.fromJson(json["EUR"]),
        gbp: json["GBP"] == null ? null : Ars.fromJson(json["GBP"]),
        ars: json["ARS"] == null ? null : Ars.fromJson(json["ARS"]),
        btc: json["BTC"] == null ? null : Ars.fromJson(json["BTC"]),
      );

  Map<String, dynamic> toJson() => {
        "source": source == null ? null : source,
        "USD": usd == null ? null : usd.toJson(),
        "EUR": eur == null ? null : eur.toJson(),
        "GBP": gbp == null ? null : gbp.toJson(),
        "ARS": ars == null ? null : ars.toJson(),
        "BTC": btc == null ? null : btc.toJson(),
      };
}

class Ars {
  Ars({
    this.name,
    this.buy,
    this.sell,
    this.variation,
  });

  String name;
  double buy;
  double sell;
  double variation;

  factory Ars.fromJson(Map<String, dynamic> json) => Ars(
        name: json["name"] == null ? null : json["name"],
        buy: json["buy"] == null ? null : json["buy"].toDouble(),
        sell: json["sell"] == null ? null : json["sell"].toDouble(),
        variation:
            json["variation"] == null ? null : json["variation"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "buy": buy == null ? null : buy,
        "sell": sell == null ? null : sell,
        "variation": variation == null ? null : variation,
      };
}
