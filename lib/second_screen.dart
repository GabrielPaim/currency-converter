import 'dart:convert';

import 'package:currency_converter/third_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'result.dart';

class SecondScreen extends StatelessWidget {
  Result data;

  Future<Result> getData() async {
    Result _result;
    var body;
    try {
      final response = await http
          .get('https://api.hgbrasil.com/finance?format=json&key=4177407c');

      if (response.statusCode == 200) {
        body = jsonDecode(response.body)['results'];
        _result = Result.fromJson(body);
        data = _result;
      }
    } catch (e) {
      print('error fetching data: $e');
      return null;
    }
    return _result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: FutureBuilder(
            future: getData(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Escolha a moeda:',
                      style: TextStyle(
                        fontSize: 24,
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    CurrencyCard('USD', data.currencies.usd),
                    CurrencyCard('EUR', data.currencies.eur),
                    CurrencyCard('GBP', data.currencies.gbp),
                    CurrencyCard('ARS', data.currencies.ars),
                    CurrencyCard('BTC', data.currencies.btc),
                    Spacer(
                      flex: 4,
                    ),
                  ],
                );
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ),
      ),
    );
  }
}

class CurrencyCard extends StatelessWidget {
  CurrencyCard(this.currency, this.ars);
  Ars ars;
  String currency;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ThirdScreen(ars),
            ));
      },
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              spreadRadius: 4,
              blurRadius: 4,
            ),
          ],
          color: Colors.white,
        ),
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              currency,
              style: TextStyle(
                fontSize: 24,
              ),
            ),
            Text(
              ars.name,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            Text(
              'Cotação atual: R\$ ${ars.buy.toStringAsFixed(2)}',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
