import 'package:currency_converter/result.dart';
import 'package:currency_converter/second_screen.dart';
import 'package:flutter/material.dart';

class ThirdScreen extends StatefulWidget {
  ThirdScreen(this.currency);
  final Ars currency;

  @override
  _ThirdScreenState createState() => _ThirdScreenState();
}

class _ThirdScreenState extends State<ThirdScreen> {
  TextEditingController _realController = TextEditingController(text: '1.00');

  double valor;

  @override
  Widget build(BuildContext context) {
    if (valor == null) valor = widget.currency.buy;

    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Moedas:',
                style: TextStyle(
                  fontSize: 24,
                ),
              ),
              Spacer(
                flex: 1,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text('Real'),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: TextField(
                      autofocus: true,
                      onChanged: (v) {
                        setState(() {
                          valor = double.parse(v) * widget.currency.buy;
                        });
                      },
                      keyboardType: TextInputType.number,
                      controller: _realController,
                    ),
                  ),
                  Text('${widget.currency.name}'),
                  Text('${valor.toStringAsFixed(2)}'),
                ],
              ),
              Spacer(
                flex: 4,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
