import 'package:currency_converter/second_screen.dart';
import 'package:flutter/material.dart';

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(
              flex: 6,
            ),
            Text(
              'Bem vindo ao',
              style: TextStyle(
                fontSize: 32,
              ),
            ),
            Text(
              'Meu conversor',
              style: TextStyle(
                fontSize: 36,
                color: Colors.greenAccent.shade700,
              ),
            ),
            Spacer(),
            FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondScreen(),
                    ));
              },
              child: Text(
                'Entrar',
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                ),
              ),
              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 128),
              color: Colors.greenAccent.shade700,
            ),
            Spacer(
              flex: 10,
            ),
          ],
        ),
      ),
    );
  }
}
